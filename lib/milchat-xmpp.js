/* Xmpp module - implements federation */

var util = require('util');
var xmpp = require('node-xmpp');
var pem = require('pem');

//attach error event so it does not throw fatal
var oldConnectionError = xmpp.Connection.prototype.error;
xmpp.Connection.prototype.error = function () {
	this.once('error', function () { });
	
	return oldConnectionError.apply(this, arguments);
};

var notifyWithExternalMessage = function (stanza, core, type) {
	var message = stanza.getChild('body').getText();
	var from;
	var to;
	
	try {
		from = new xmpp.JID(stanza.attrs.from);
		to = new xmpp.JID(stanza.attrs.to);
	} catch (e) {
		return false;
	}
	
	if (!to.getLocal() || to.getDomain() !== core.domain) {
		return false;
	}
	
	var recipient = core.findBy(core.users, 'name', to.getLocal().toLowerCase(), true);
	if (recipient === undefined) {
		return false;
	}
	
	if (recipient.notifiedBy === undefined) {
		recipient.notifiedBy = {};
	}
	
	var fromId = (new xmpp.JID(from.getLocal(), from.getDomain())).toString();
	
	if (recipient.state === recipient.states.inChat && recipient.chatter.getLocal() === from.getLocal() && recipient.chatter.getDomain() === from.getDomain()) {
		if (type === 'error') {
			recipient.emit('notify', { message: core.texts.userBusyForChat, type: 'error' });
		} else {
			recipient.emit('notify', { message: util.format(core.texts.chatUserSays, from.toString(), message) });
			recipient.notifiedBy[fromId] = false;
		}
		return true;
	} else if (!recipient.notifiedBy[fromId]) {
		recipient.emit('notify', { message: util.format(core.texts.userWantsToChat, fromId) });
		recipient.notifiedBy[fromId] = true;
		return false;
	} else {
		return false;
	}
};

var init = function (core, port) {
	if (core.User.prototype.states.inChat === undefined) {
		console.error('Cannot load xmpp; please load chat feature first.');
		return core;
	}
	
	port = port || 5269;
	
	var r = new xmpp.Router(port);
	var xmppReady = false;
	
	core.xmppRouter = r;
	
	core.texts.xmppNotReady = 'service is not ready';
	
	core.ee.first('command', function (data, done) {
		var commandParts = data.commandParts;
		var user = data.user;
		
		if (user.state !== user.states.logged) {
			done();
			return;
		}
		
		if (commandParts.length === 2 && commandParts[0] === 'chat') {
			var jid;
			
			try {
				jid = new xmpp.JID(commandParts[1]);
			} catch (e) {
				done();
				return;
			}
			
			if (jid.getLocal() && jid.getDomain() !== core.domain) {
				user.emit('notify', { message: util.format(core.texts.userJoinedChat, jid.toString()) });
				
				//join chat
				user.chatter = jid;
				user.state = user.states.inChat;
				return;
			}
		}
		
		done();
	});
	
	core.ee.first('command', function (data, done) {
		var commandParts = data.commandParts;
		var user = data.user;
		
		if (user.state !== user.states.inChat) {
			done();
			return;
		}
		
		if (commandParts[0] === 'leave') {
			if (user.chatter.getDomain() !== core.domain) {
				user.emit('notify', { message: util.format(core.texts.userLeavingChat, user.chatter.toString()) });
				
				user.chatter = undefined;
				user.state = user.states.logged;
				return;
			}
		}
		
		done();
	});
	
	core.ee.first('message', function (data, done) {
		var message = data.message;
		var user = data.user;
		
		if (user.state !== user.states.inChat) {
			done();
			return;
		}
		
		if (user.chatter.getDomain() === core.domain) {
			done();
			return;
		}
		
		if (!xmppReady) {
			user.emit('notify', { message: core.texts.xmppNotReady, type: 'error' });
			return;
		}
		
		var stanza = new xmpp.Element(
			'message',
			{
				to: user.chatter.toString(),
				type: 'chat',
				from: (new xmpp.JID(user.name, user.getDomain())).toString()
			}
		);
		stanza.c('body').t(message);
		r.send(stanza);
	});
	
	pem.createCertificate({
		days: 100,
		selfSigned: true,
		organization: core.domain,
		organizationUnit: 'chat',
		commonName: core.domain
	}, function (error, keys) {
		if (error) {
			console.error(error);
		} else {
			xmppReady = true;
			console.log('Xmpp server listening on port', port);
			
			r.loadCredentials(
				core.domain,
				keys.serviceKey,
				keys.certificate
			);

			r.register(core.domain, function (stanza) {
				if (stanza.attrs.type === 'error') {
					if (!notifyWithExternalMessage(stanza, core, 'error')) {
						console.error(stanza.toString());
					}
				}
				
				if (stanza.attrs.type === 'chat') {
					var delivered = notifyWithExternalMessage(stanza, core);
					if (!delivered) {
						var error = new xmpp.Element(
							'message',
							{ to: stanza.from, type: 'error', from: stanza.to }
						);
						error.c('body').t(stanza.getChild('body').getText());
						error.c('error', { type: 'wait'})
							.c('recipient-unavailable');
						
						r.send(error);
					}
				}
			});
		}
	});
	
	return core;
};

exports.init = init;