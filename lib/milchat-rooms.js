/* Rooms module - implements conversations with other users using named rooms */
/* commands: /rooms, /join $room, /leave */

var util = require('util');
var AsyncEventEmitter = require('async-eventemitter');

var Room = function (name) {
	this.name = name;
	this.members = [];
};
util.inherits(Room, AsyncEventEmitter);

var texts;

var notifyRoom = function (data, done) {
	var message = data.message;
	var from = data.from;
	
	this.members.forEach(function (member) {
		if (from !== undefined) {
			member.emit('notify', { message: util.format(texts.roomUserSays, from.name, message) });
		} else {
			member.emit('notify', { message: util.format(texts.roomSystemSays, message) });
		}
	});
	
	done();
};

var leaveRoom = function (data, done) {
	var user = data.user;
	
	user.state = user.states.logged;
	user.room = undefined;
	
	this.members.splice(this.members.indexOf(user), 1);
	
	this.emit('notify', { message: util.format(texts.userLeftRoom, user.name) });
	
	done();
};

var init = function (core, roomNames) {
	roomNames = roomNames || [];
	
	if (core.User.prototype.states.logged === undefined) {
		console.error('Cannot load rooms; please load login feature first.');
		return core;
	}
	
	//extend
	core.User.prototype.states.inRoom = Object.keys(core.User.prototype.states).length;
	core.texts.roomsListBegin = 'Active rooms are';
	core.texts.roomsListItem = '* %s (%d)';
	core.texts.roomsListEnd = 'end of list';
	core.texts.roomNotFound = 'room does not exist';
	core.texts.userJoinedRoom = 'new user joined chat: %s';
	core.texts.userEnteringRoom = 'entering room: %s';
	core.texts.usersListItem = '* %s';
	core.texts.usersListItemSelf = '* %s (** this is you)';
	core.texts.usersListEnd = 'end of list';
	core.texts.roomUserSays = '%s: %s';
	core.texts.roomSystemSays = '* %s';
	core.texts.userLeftRoom = 'user has left room: %s';
	core.texts.userLeavingRoom = 'leaving room: %s';
	texts = core.texts;
	
	core.rooms = [];
	roomNames.forEach(function (name) {
		var room = new Room(name);
		room.on('notify', notifyRoom);
		room.on('leave', leaveRoom);
		
		core.rooms.push(room);
	});
	
	core.ee.on('newUser', function (data, done) {
		var user = data.user;
		
		user.on('cleanUser', function (data, done) {
			if (user.room !== undefined) {
				user.room.emit('leave', { user: user });
			}
			
			done();
		});
		
		done();
	});
	
	core.ee.on('command', function (data, done) {
		var commandParts = data.commandParts;
		var user = data.user;
		
		if (user.state !== user.states.logged) {
			done();
			return;
		}
		
		if (commandParts[0] === 'rooms') {
			user.emit('notify', { message: core.texts.roomsListBegin });
			core.rooms.forEach(function (room) {
				user.emit('notify', { message: util.format(core.texts.roomsListItem, room.name, room.members.length) });
			});
			user.emit('notify', { message: core.texts.roomsListEnd });
		} else if (commandParts.length === 2 && commandParts[0] === 'join') {
			var roomName = commandParts[1];
			var room = core.findBy(core.rooms, 'name', roomName);
			
			if (room === undefined) {
				user.emit('notify', { message: core.texts.roomNotFound, type: 'error' });
				return;
			}
			
			room.emit('notify', { message: util.format(core.texts.userJoinedRoom, user.name) });
			
			//join room
			user.room = room;
			user.state = user.states.inRoom;
			room.members.push(user);
			
			user.emit('notify', { message: util.format(core.texts.userEnteringRoom, room.name) });
			
			//display list of users
			room.members.forEach(function (member) {
				if (member === user) {
					user.emit('notify', { message: util.format(core.texts.usersListItemSelf, member.name) });
				} else {
					user.emit('notify', { message: util.format(core.texts.usersListItem, member.name) });
				}
			});
			user.emit('notify', { message: core.texts.usersListEnd });
		}
		
		done();
	});
	
	core.ee.on('command', function (data, done) {
		var commandParts = data.commandParts;
		var user = data.user;
		
		if (user.state !== user.states.inRoom) {
			done();
			return;
		}
		
		if (commandParts[0] === 'leave') {
			var room = user.room;
			room.emit('leave', { user: user });
			user.emit('notify', { message: util.format(core.texts.userLeavingRoom, room.name) });
		}
		
		done();
	});
	
	core.ee.on('message', function (data, done) {
		var message = data.message;
		var user = data.user;
		
		if (user.state !== user.states.inRoom) {
			done();
			return;
		}
		
		user.room.emit('notify', { message: message, from: user });
		done();
	});

	
	return core;
};

exports.init = init;