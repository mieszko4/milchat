/* Chat module - implements a private conversation with other user */
/* commands: /chat, /leave */

var util = require('util');

var init = function (core) {
	if (core.User.prototype.states.logged === undefined) {
		console.error('Cannot load chat; please load login feature first.');
		return core;
	}
	
	//extend
	core.User.prototype.states.inChat = Object.keys(core.User.prototype.states).length;
	core.texts.chatterNotFound = 'user does not exist';
	core.texts.chatterSelf = 'you cannot chat with yourself';
	core.texts.userWantsToChat = '* %s wants to have private chat with you';
	core.texts.userJoinedChat = 'you joined private chat with: %s';
	core.texts.userLeftChat = '* %s left private chat with you';
	core.texts.userLeavingChat = 'you are leaving private chat with: %s';
	core.texts.userBusyForChat = 'chatter did not get your message';
	core.texts.chatUserSays = '%s: %s';
	
	core.ee.on('newUser', function (data, done) {
		var user = data.user;
		
		user.on('cleanUser', function (data, done) {
			var chatter = core.findBy(core.users, 'chatter', user);
			if (chatter !== undefined) {
				chatter.emit('notify', { message: util.format(core.texts.userLeftChat, user.name) });
			}
			
			done();
		});
		
		done();
	});
	
	core.ee.on('command', function (data, done) {
		var commandParts = data.commandParts;
		var user = data.user;
		
		if (user.state !== user.states.logged) {
			done();
			return;
		}
		
		if (commandParts.length === 2 && commandParts[0] === 'chat') {
			var chatterName = commandParts[1];
			var chatter = core.findBy(core.users, 'name', chatterName.toLowerCase(), true);
			
			if (chatter === undefined) {
				user.emit('notify', { message: core.texts.chatterNotFound, type: 'error' });
				return;
			}
			
			if (chatter === user) {
				user.emit('notify', { message: core.texts.chatterSelf, type: 'error' });
				return;
			}
			
			user.emit('notify', { message: util.format(core.texts.userJoinedChat, chatter.name) });
			chatter.emit('notify', { message: util.format(core.texts.userWantsToChat, user.name) });
			
			//join chat
			user.chatter = chatter;
			user.state = user.states.inChat;
		}
		
		done();
	});
	
	core.ee.on('command', function (data, done) {
		var commandParts = data.commandParts;
		var user = data.user;
		
		if (user.state !== user.states.inChat) {
			done();
			return;
		}
		
		if (commandParts[0] === 'leave') {
			if (user.chatter.chatter === user) {
				user.chatter.emit('notify', { message: util.format(core.texts.userLeftChat, user.name) });
			}
			user.emit('notify', { message: util.format(core.texts.userLeavingChat, user.chatter.name) });
			
			user.chatter = undefined;
			user.state = user.states.logged;
		}
		
		done();
	});
	
	core.ee.on('message', function (data, done) {
		var message = data.message;
		var user = data.user;
		
		if (user.state !== user.states.inChat) {
			done();
			return;
		}
		
		if (user.chatter.state === user.states.inChat) {
			user.chatter.emit('notify', { message: util.format(core.texts.chatUserSays, user.name, message) });
		} else {
			user.emit('notify', { message: core.texts.userBusyForChat, type: 'error' });
		}
		
		done();
	});

	
	return core;
};

exports.init = init;