/* Main module - implements core elements */
/* commands: /quit */

var net = require('net');
var util = require('util');
var AsyncEventEmitter = require('async-eventemitter');

var eol = '\r\n';
var encoding = 'utf-8';
var commandPrefix = '/';
var escapeCommandPrefixValidator = new RegExp('^\\/\\/');

var texts = {
	welcome: 'Welcome to the Milchat server',
	bye: 'BYE',
	errorPrefix: 'Sorry, '
};

var User = function (socket) {
	this.socket = socket;
	this.name = undefined;
	this.state = this.states.initial;
};
util.inherits(User, AsyncEventEmitter);
User.prototype.getDomain = function () {
	return this.domain;
};

var globals = {};

var notifyUser = function (data, done) {
	var message = data.message;
	var type = data.type;
	
	if (type === 'error') {
		message = texts.errorPrefix + message;
	}
	
	this.socket.write(message + eol);
	done();
};

var server = net.createServer(function (socket) {
	var packet = '';
	var user = new User(socket);
	
	user.on('notify', notifyUser);
	socket.setEncoding(encoding);
	
	//socket events
	socket.on('data', function (buffer) {
		var packets = buffer.toString().split(eol);
		
		if (packets.length === 1) {
			packet += packets[0];
		} else {
			socket.emit('packet', packet + packets[0]);
			
			for (var i = 1, l = packets.length - 1; i < l; i++) {
				socket.emit('packet', packets[i]);
			}
			
			packet = packets[packets.length - 1];
		}
		
	});
	socket.on('packet', function (message) {
		if (message.indexOf(commandPrefix) === 0 && !message.match(escapeCommandPrefixValidator)) {
			globals.ee.emit('command', { commandParts: message.substring(commandPrefix.length).split(' '), user: user });
		} else {
			globals.ee.emit('message', { message: message.replace(escapeCommandPrefixValidator, commandPrefix), user: user });
		}
	});
	socket.on('end', function () {
		user.emit('cleanUser');
		console.log('Server disconnected user', user.name);
	});
	
	user.emit('notify', { message: texts.welcome });
	globals.ee.emit('newUser', { user: user });
});

var init = function (port, domain) {
	port = port || 9399;
	domain = domain || 'localhost';
	globals.domain = domain;
	User.prototype.domain = domain;
	User.prototype.states = {
		initial: 0
	};
	
	globals.ee = new AsyncEventEmitter();
	globals.ee.on('command', function (data, done) {
		var commandParts = data.commandParts;
		var user = data.user;
		
		if (commandParts[0] === 'quit') {
			user.emit('notify', { message: texts.bye });
			user.socket.end();
			return;
		}
		
		done();
	});
	
	server.listen(port, function () {
		console.log('Server listening on port', port);
	});
	
	exports.ee = globals.ee;
	exports.domain = globals.domain;
};

var findBy = function (objects, property, value, lowerCase) {
	var found;
	lowerCase = lowerCase || false;
	
	objects.some(function (obj) {
		var objValue = lowerCase ? obj[property].toLowerCase() : obj[property];
		var condition = objValue === value;
		if (condition) {
			found = obj;
		}
		
		return condition;
	});
	
	return found;
};

exports.User = User;
exports.init = init;
exports.server = server;
exports.texts = texts;

exports.findBy = findBy;
