/* Websocket module - implements service to be used by websocket clients */

var app = require('http').createServer(handler);
var io = require('socket.io')(app);
var fs = require('fs');
var util = require('util');

function handler (req, res) {
  fs.readFile(__dirname + '/../webclient/index.html',
  function (err, data) {
    if (err) {
      res.writeHead(500);
      return res.end('Error loading index.html');
    }

    res.writeHead(200);
    res.end(data);
  });
}

var notifyUser = function (data, done) {
	this.socket.emit('notify', data);
	done();
};

var init = function (core, port) {
	if (core.User.prototype.states.logged === undefined) {
		console.error('Cannot load websocket; please load login feature first.');
		return core;
	}
	
	port = port || 9400;
	
	app.listen(port, function () {
		console.log('Websocket server listening on port', port);
	});
	
	core.io = io;
	
	io.on('connection', function (socket) {
		var user = new core.User(socket);
		user.on('notify', notifyUser);
		
		socket.on('message', function (data) {
			if (user.state === user.states.initial) {
				return; //ignore
			}
			
			data.user = user;
			core.ee.emit('message', data);
		});
		
		socket.on('login', function (name) {
			if (user.state !== user.states.initial) {
				return; //ignore
			}
			
			var data = {
				message: name,
				user: user
			};
			core.ee.emit('message', data);
		});
		
		socket.on('logout', function () {
			if (user.state !== user.states.logged) {
				return; //ignore
			}
			
			user.emit('cleanUser');
			user.state = user.states.initial;
			console.log('Server logged out user', user.name);
		});
		
		socket.on('disconnect', function() {
			user.emit('cleanUser');
			console.log('Server disconnected user', user.name);
		});
		
		socket.on('command', function (data) {
			if (data.commandParts && data.commandParts[0] === 'quit') {
				return; //ignore
			}
			
			data.user = user;
			core.ee.emit('command', data);
		});
		
		user.emit('notify', { message: core.texts.welcome });
		core.ee.emit('newUser', { user: user });
	});
	
	return core;
};

exports.init = init;