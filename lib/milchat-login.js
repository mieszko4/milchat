/* Login module - checks for unique and valid username */

var RegExp = require('xregexp').XRegExp;
var util = require('util');

var usernameValidator = new RegExp('^[\\p{L}0-9_]{3,}$');

var init = function (core) {
	//extend
	core.User.prototype.states.logged = Object.keys(core.User.prototype.states).length;
	core.texts.enterLogin = 'Login Name?';
	core.texts.nameInvalid = 'name must consist of at least 3 characters (letter, digit, underscore)';
	core.texts.nameTaken = 'name taken';
	core.texts.welcomeUser = 'Welcome %s!';
	
	core.users = [];
	
	core.ee.on('newUser', function (data, done) {
		var user = data.user;
		
		user.emit('notify', { message: core.texts.enterLogin });
		done();
	});
	
	core.ee.on('message', function (data, done) {
		var name = data.message;
		var user = data.user;
		
		if (user.state !== user.states.initial) {
			done();
			return;
		}
		
		//is name valid
		if (!name.match(usernameValidator)) {
			user.emit('notify', { message: core.texts.nameInvalid, type: 'error' });
			user.emit('notify', { message: core.texts.enterLogin });
			return;
		}
		
		//is name taken
		if (core.findBy(core.users, 'name', name.toLowerCase(), true) !== undefined) {
			user.emit('notify', { message: core.texts.nameTaken, type: 'error' });
			user.emit('notify', { message: core.texts.enterLogin });
			return;
		}
		
		//login user
		user.name = name;
		user.state = user.states.logged;
		core.users.push(user);
		
		user.once('cleanUser', function (data, done) {
			//logout user
			core.users.splice(core.users.indexOf(user), 1);
			done();
		});
		
		user.emit('notify', { message: util.format(core.texts.welcomeUser, user.name) });
		console.log('Server connected user', user.name);
		
		done();
	});
	
	return core;
};

exports.init = init;