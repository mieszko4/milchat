var should = require('should');
var net = require('net');
var util = require('util');

var core = require('./../lib/milchat-core');
var login = require('./../lib/milchat-login');
var rooms = require('./../lib/milchat-rooms');
var chat = require('./../lib/milchat-chat');

describe('Chat', function () {
	var port = 9398;
	var domain = 'milosz.ch';
	var eol = '\r\n';
	var chatCommand = '/chat';
	var leaveCommand = '/leave';
	var timeout = 100;
	var userPrefix = 'milosz_';
	var clients = [];
	var clientsAmount = 5;
	
	before(function (done) {
		core.init(port, domain);
		login.init(core);
		rooms.init(core, []);
		chat.init(core);
		
		core.users.length.should.equal(0);
		for (var i = 0; i < clientsAmount; i++) {
			(function (i) {
				var name = userPrefix + i;
				clients[i] = net.connect({ port: port }, function() {
					this.write(name + eol);
				});
			}(i));
		}
		
		setTimeout(function () {
			core.users.length.should.equal(clientsAmount);
			done();
		}, timeout);
	});
	
	after(function () {
		core.server.close();
	});
	
	describe('#chat and #leave', function () {
		it('should not chat with self', function (done) {
			var response = '';
			
			var user = core.findBy(core.users, 'name', userPrefix + 0);
			(user !== undefined).should.be.true;
			user.state.should.equal(user.states.logged);
			
			clients[0].write(chatCommand + ' ' + userPrefix + 0 + eol);
			
			clients[0].on('data', function (data) {
				response += data.toString();
			});
			
			setTimeout(function () {
				user.state.should.equal(user.states.logged);
				response.should.endWith(core.texts.errorPrefix + core.texts.chatterSelf + eol);
				done();
			}, timeout);
		});
		it('should not chat with unknown user', function (done) {
			var response = '';
			
			var user = core.findBy(core.users, 'name', userPrefix + 0);
			(user !== undefined).should.be.true;
			user.state.should.equal(user.states.logged);
			
			clients[0].write(chatCommand + ' ' + 'nonexistingUser' + eol);
			
			clients[0].on('data', function (data) {
				response += data.toString();
			});
			
			setTimeout(function () {
				user.state.should.equal(user.states.logged);
				response.should.endWith(core.texts.errorPrefix + core.texts.chatterNotFound + eol);
				done();
			}, timeout);
		});
		it('should join chat with known user', function (done) {
			var response = '';
			var chatterName = userPrefix + 1;
			
			var user = core.findBy(core.users, 'name', userPrefix + 0);
			(user !== undefined).should.be.true;
			user.state.should.equal(user.states.logged);
			
			var chatter = core.findBy(core.users, 'name', chatterName);
			(chatter !== undefined).should.be.true;
			
			clients[0].write(chatCommand + ' ' + chatterName + eol);
			
			clients[0].on('data', function (data) {
				response += data.toString();
			});
			
			setTimeout(function () {
				user.state.should.equal(user.states.inChat);
				chatter.should.equal(user.chatter);
				response.should.endWith(util.format(core.texts.userJoinedChat, chatterName) + eol);
				done();
			}, timeout);
		});
		it('should leave chat with known user', function (done) {
			var response = '';
			var chatterName = userPrefix + 1;
			
			var user = core.findBy(core.users, 'name', userPrefix + 0);
			var chatter = core.findBy(core.users, 'name', chatterName);
			
			clients[0].write(leaveCommand + eol);
			
			clients[0].on('data', function (data) {
				response += data.toString();
			});
			
			setTimeout(function () {
				user.state.should.equal(user.states.logged);
				response.should.endWith(util.format(core.texts.userLeavingChat, chatterName) + eol);
				done();
			}, timeout);
		});
	});
	
	describe('#chat and talk', function () {
		it('should join chat with known user', function (done) {
			var responses = [];
			var username1 = userPrefix + 0;
			var username2 = userPrefix + 1;
			var user1 = core.findBy(core.users, 'name', username1);
			var user2 = core.findBy(core.users, 'name', username2);
			
			user1.state.should.equal(user1.states.logged);
			
			clients.forEach(function (client, i) {
				responses[i] = '';
				client.on('data', function (data) {
					responses[i] += data.toString();
				});
			});
			
			clients[0].write(chatCommand + ' ' + username2 + eol);
			
			setTimeout(function () {
				user1.state.should.equal(user1.states.inChat);
				user2.should.equal(user1.chatter);
				responses[0].should.endWith(util.format(core.texts.userJoinedChat, username2) + eol);
				responses[1].should.endWith(util.format(core.texts.userWantsToChat, username1) + eol);
				done();
			}, timeout);
		});
		it('should not be able to send message', function (done) {
			var responses = [];
			var username1 = userPrefix + 0;
			var username2 = userPrefix + 1;
			var messageFormat = 'Hello, I am client #%s';
			
			var user1 = core.findBy(core.users, 'name', username1);
			var user2 = core.findBy(core.users, 'name', username2);
			
			user1.state.should.equal(user1.states.inChat);
			
			clients.forEach(function (client, i) {
				responses[i] = '';
				client.on('data', function (data) {
					responses[i] += data.toString();
				});
			});
			
			clients[0].write(util.format(messageFormat, username1) + eol);
			
			setTimeout(function () {
				responses[0].should.endWith(core.texts.errorPrefix + core.texts.userBusyForChat + eol);
				responses[1].should.be.empty;
				done();
			}, timeout);
		});
		it('should join chat who has already joined chat', function (done) {
			var responses = [];
			var username1 = userPrefix + 0;
			var username2 = userPrefix + 1;
			var user1 = core.findBy(core.users, 'name', username1);
			var user2 = core.findBy(core.users, 'name', username2);
			
			user2.state.should.equal(user1.states.logged);
			
			clients.forEach(function (client, i) {
				responses[i] = '';
				client.on('data', function (data) {
					responses[i] += data.toString();
				});
			});
			
			clients[1].write(chatCommand + ' ' + username1 + eol);
			
			setTimeout(function () {
				user2.state.should.equal(user1.states.inChat);
				user1.should.equal(user2.chatter);
				responses[0].should.endWith(util.format(core.texts.userWantsToChat, username2) + eol);
				responses[1].should.endWith(util.format(core.texts.userJoinedChat, username1) + eol);
				done();
			}, timeout);
		});
		it('should talk to each other', function (done) {
			var responses = [];
			var username1 = userPrefix + 0;
			var username2 = userPrefix + 1;
			var messageFormat = 'Hello, I am client #%s';
			var user1 = core.findBy(core.users, 'name', username1);
			var user2 = core.findBy(core.users, 'name', username2);
			
			user1.state.should.equal(user1.states.inChat);
			user2.state.should.equal(user1.states.inChat);
			
			clients.forEach(function (client, i) {
				responses[i] = '';
				client.on('data', function (data) {
					responses[i] += data.toString();
				});
			});
			
			clients[0].write(util.format(messageFormat, username1) + eol);
			clients[1].write(util.format(messageFormat, username2) + eol);
			
			setTimeout(function () {
				responses[0].should.endWith(util.format(messageFormat, username2) + eol);
				responses[1].should.endWith(util.format(messageFormat, username1) + eol);
				
				//other clients should not see it
				clients.forEach(function (client, i) {
					if (i > 1) {
						responses[i].should.not.endWith(util.format(messageFormat, username2) + eol);
						responses[i].should.not.endWith(util.format(messageFormat, username1) + eol);
					}
				});
				done();
			}, timeout);
		});
	});
});
