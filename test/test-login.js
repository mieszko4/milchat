var should = require('should');
var net = require('net');
var util = require('util');

var core = require('./../lib/milchat-core');
var login = require('./../lib/milchat-login');

describe('Login', function () {
	var port = 9398;
	var domain = 'milosz.ch';
	var eol = '\r\n';
	var quitCommand = '/quit';
	var timeout = 100;
	var userPrefix = 'milosz_';
	var sharedClient;
	
	before(function () {
		core.init(port, domain);
		login.init(core);
	});
	
	after(function () {
		core.server.close();
	});
	
	describe('#login', function () {
		it('should be able to login and quit some clients', function (done) {
			var clientsAmount = 10;
			var loggersAmount = 7;
			var loggerCounter = 0;
			
			var responses = [];
			for (var i = 0; i < clientsAmount; i++) {
				(function (i) {
					responses[i] = '';
					var client = net.connect({ port: port }, function() {
						var name = userPrefix + i;
						this.write(name + eol);
						
						if (loggerCounter < loggersAmount) {
							loggerCounter++;
							
							setTimeout(function () {
								responses[i].should.endWith(util.format(core.texts.welcomeUser, name) + eol);
								
								setTimeout(function () {
									client.write(quitCommand + eol);
								}, timeout);
							}, timeout);
						}
					});
				
					client.on('data', function (data) {
						responses[i] += data.toString();
					});
					
					client.on('end', function() {
						responses[i].should.endWith(core.texts.bye + eol);
						loggerCounter--;
						
						if (loggerCounter <= 0) {
							done();
						}
					});
				}(i));
			}
		});
		it('should have 3 users logged in', function (done) {
			core.users.length.should.equal(3);
			done();
		});
		it('should show error that name is taken', function (done) {
			var name = userPrefix.toUpperCase() + '9';
			var response = '';
			var client = net.connect({ port: port }, function() {
				this.write(name + eol);
				
				setTimeout(function () {
					response.should.endWith(core.texts.errorPrefix + core.texts.nameTaken + eol + core.texts.enterLogin + eol);
					var user = core.findBy(core.users, 'name', name);
					(user !== undefined).should.be.false;
					done();
				}, timeout);
			});
			
			client.on('data', function (data) {
				response += data.toString();
			});
		});
		it('should have 3 users logged', function (done) {
			core.users.length.should.equal(3);
			done();
		});
		it('should log in user', function (done) {
			var name = userPrefix + '10';
			
			core.users.length.should.equal(3);
			var user = core.findBy(core.users, 'name', name);
			(user !== undefined).should.be.false;
			
			var client = net.connect({ port: port }, function() {
				this.write(name + eol);
				sharedClient = this;
				
				setTimeout(function () {
					core.users.length.should.equal(4);
					var user = core.findBy(core.users, 'name', name);
					(user !== undefined).should.be.true;
					user.state.should.equal(user.states.logged);
					done();
				}, timeout);
			});
		});
		it('should log out user on quit', function (done) {
			var name = userPrefix + '10';
			
			core.users.length.should.equal(4);
			var user = core.findBy(core.users, 'name', name);
			(user !== undefined).should.be.true;
			user.state.should.equal(user.states.logged);
			
			sharedClient.write(quitCommand + eol);
			
			setTimeout(function () {
				core.users.length.should.equal(3);
				var user = core.findBy(core.users, 'name', name);
				(user !== undefined).should.be.false;
				done();
			}, timeout);
		});
	});
});
