var should = require('should');
var net = require('net');
var util = require('util');
var socketIoClient = require('socket.io-client');

var core = require('./../lib/milchat-core');
var login = require('./../lib/milchat-login');
var rooms = require('./../lib/milchat-rooms');
var chat = require('./../lib/milchat-chat');
var xmpp = require('./../lib/milchat-xmpp');
var websocket = require('./../lib/milchat-websocket');

describe('Websocket', function () {
	var port = 9398;
	var xmppPort = 5267; //standard port: 5269
	var websocketPort = 9397;
	var domain = 'milosz.ch';
	var eol = '\r\n';
	var roomsCommand = 'rooms';
	var joinCommand = 'join';
	var chatCommand = 'chat';
	var leaveCommand = 'leave';
	var roomNames = ['room1', 'room2', 'room3'];
	var timeout = 100;
	var telnetClientName = 'telnet';
	var telnetClient;
	var webClientName = 'websocket';
	var webClient;
	
	before(function (done) {
		core.init(port, domain);
		login.init(core);
		rooms.init(core, roomNames);
		chat.init(core);
		xmpp.init(core, xmppPort);
		websocket.init(core, websocketPort);
		
		telnetClient = net.connect({ port: port }, function() {
			this.write(telnetClientName + eol);
		});
		webClient = socketIoClient('http://localhost:' + websocketPort);
		
		setTimeout(function () {
			done();
		}, timeout);
	});
	
	after(function () {
		core.server.close();
		////core.xmppRouter.close();
		core.io.close();
	});
	
	describe('#login and #logout', function () {
		it('should login', function (done) {
			webClient.emit('login', webClientName);
			
			setTimeout(function () {
				core.users.length.should.equal(2);
				var user = core.findBy(core.users, 'name', webClientName);
				(user !== undefined).should.be.true;
				user.state.should.equal(user.states.logged);
				
				done();
			}, timeout);
		});
		it('should logout', function (done) {
			webClient.emit('logout');
			
			setTimeout(function () {
				core.users.length.should.equal(1);
				
				done();
			}, timeout);
		});
	});
	
	describe('messages', function () {
		it('should send notifications', function (done) {
			var responses = [];
			webClient.emit('login', telnetClientName);
			
			webClient.on('notify', function (message) {
				responses.push(message);
			});
			
			setTimeout(function () {
				responses.length.should.equal(2);
				(responses[0].message !== undefined).should.be.true;
				(responses[0].type !== undefined).should.be.true;
				responses[0].message.should.equal(core.texts.nameTaken);
				
				(responses[1].message !== undefined).should.be.true;
				(!responses[1].type).should.be.true;
				responses[1].message.should.equal(core.texts.enterLogin);
				
				done();
			}, timeout);
		});
	});
	
	describe('commands', function () {
		it('should log in', function (done) {
			var responses = [];
			webClient.emit('login', webClientName);
			
			webClient.on('notify', function (message) {
				responses.push(message);
			});
			
			setTimeout(function () {
				core.users.length.should.equal(2);
				var user = core.findBy(core.users, 'name', webClientName);
				(user !== undefined).should.be.true;
				user.state.should.equal(user.states.logged);
				
				done();
			}, timeout);
		});
		it('should see rooms', function (done) {
			var responses = [];
			var user = core.findBy(core.users, 'name', webClientName);
			(user !== undefined).should.be.true;
			user.state.should.equal(user.states.logged);
			
			webClient.on('notify', function (message) {
				responses.push(message);
			});
			
			webClient.emit('command', {
				commandParts: [roomsCommand]
			});
			
			setTimeout(function () {
				user.state.should.equal(user.states.logged);
				responses.length.should.not.equal(0);
				
				done();
			}, timeout);
		});
		
		it('should join room', function (done) {
			var responses = [];
			var user = core.findBy(core.users, 'name', webClientName);
			(user !== undefined).should.be.true;
			user.state.should.equal(user.states.logged);
			
			webClient.on('notify', function (message) {
				responses.push(message);
			});
			
			webClient.emit('command', {
				commandParts: [joinCommand, roomNames[1]]
			});
			
			setTimeout(function () {
				user.state.should.equal(user.states.inRoom);
				responses.length.should.not.equal(0);
				
				done();
			}, timeout);
		});
		it('should send message', function (done) {
			var responses = [];
			var testMessage = 'Test message';
			var user = core.findBy(core.users, 'name', webClientName);
			(user !== undefined).should.be.true;
			user.state.should.equal(user.states.inRoom);
			
			webClient.on('notify', function (message) {
				responses.push(message);
			});
			
			webClient.emit('message', {
				message: testMessage
			});
			
			setTimeout(function () {
				user.state.should.equal(user.states.inRoom);
				responses.length.should.not.equal(0);
				responses[0].message.should.equal(util.format(core.texts.roomUserSays, webClientName, testMessage));
				
				done();
			}, timeout);
		});
		it('should leave room', function (done) {
			var responses = [];
			var user = core.findBy(core.users, 'name', webClientName);
			(user !== undefined).should.be.true;
			user.state.should.equal(user.states.inRoom);
			
			webClient.on('notify', function (message) {
				responses.push(message);
			});
			
			webClient.emit('command', {
				commandParts: [leaveCommand]
			});
			
			setTimeout(function () {
				user.state.should.equal(user.states.logged);
				responses.length.should.not.equal(0);
				
				done();
			}, timeout);
		});
		it('should join chat', function (done) {
			var responses = [];
			var user = core.findBy(core.users, 'name', webClientName);
			(user !== undefined).should.be.true;
			user.state.should.equal(user.states.logged);
			
			webClient.on('notify', function (message) {
				responses.push(message);
			});
			
			webClient.emit('command', {
				commandParts: [chatCommand, telnetClientName]
			});
			
			setTimeout(function () {
				user.state.should.equal(user.states.inChat);
				responses.length.should.not.equal(0);
				
				done();
			}, timeout);
		});
		it('should leave chat', function (done) {
			var responses = [];
			var user = core.findBy(core.users, 'name', webClientName);
			(user !== undefined).should.be.true;
			user.state.should.equal(user.states.inChat);
			
			webClient.on('notify', function (message) {
				responses.push(message);
			});
			
			webClient.emit('command', {
				commandParts: [leaveCommand]
			});
			
			setTimeout(function () {
				user.state.should.equal(user.states.logged);
				responses.length.should.not.equal(0);
				
				done();
			}, timeout);
		});
	});
});
