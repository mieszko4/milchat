var should = require('should');
var net = require('net');
var util = require('util');

var core = require('./../lib/milchat-core');
var login = require('./../lib/milchat-login');
var rooms = require('./../lib/milchat-rooms');

describe('Rooms', function () {
	var port = 9398;
	var domain = 'milosz.ch';
	var eol = '\r\n';
	var roomsCommand = '/rooms';
	var joinCommand = '/join';
	var leaveCommand = '/leave';
	var roomNames = ['room1', 'room2', 'room3'];
	var timeout = 100;
	var userPrefix = 'milosz_';
	var clients = [];
	var clientsAmount = 5;
	
	before(function (done) {
		core.init(port, domain);
		login.init(core);
		rooms.init(core, roomNames);
		
		core.rooms.length.should.equal(roomNames.length);
		
		core.users.length.should.equal(0);			
		for (var i = 0; i < clientsAmount; i++) {
			(function (i) {
				var name = userPrefix + i;
				clients[i] = net.connect({ port: port }, function() {
					this.write(name + eol);
				});
			}(i));
		}
		
		setTimeout(function () {
			core.users.length.should.equal(clientsAmount);
			done();
		}, timeout);
	});
	
	after(function () {
		core.server.close();
	});
	
	describe('#rooms', function () {
		it('should list rooms', function (done) {
			var response = '';
			clients[0].write(roomsCommand + eol);
			
			clients[0].on('data', function (data) {
				response += data.toString();
			});
			
			setTimeout(function () {
				var roomsListValidator = new RegExp(core.texts.roomsListBegin + eol + '(.|[' + eol + '])*' + core.texts.roomsListEnd + eol + '$');
				
				response.should.match(roomsListValidator);
				done();
			}, timeout);
		});
	});
	
	describe('#join and #leave', function () {
		it('should join room', function (done) {
			var response = '';
			var room = core.findBy(core.rooms, 'name', roomNames[0]);
			(room !== undefined).should.be.true;
			room.members.length.should.equal(0);
			
			var user = core.findBy(core.users, 'name', userPrefix + 0);
			(user !== undefined).should.be.true;
			(user.room !== undefined).should.be.false;
			user.state.should.equal(user.states.logged);
			
			clients[0].write(joinCommand + ' ' + roomNames[0] + eol);
			
			clients[0].on('data', function (data) {
				response += data.toString();
			});
			
			setTimeout(function () {
				room.members.length.should.equal(1);
				room.members[0].should.equal(user);
				user.room.should.equal(room);
				user.state.should.equal(user.states.inRoom);
				
				var membersListValidator = new RegExp('(.|[' + eol + '])*' + core.texts.usersListEnd + eol + '$');
				response.should.match(membersListValidator);
				
				done();
			}, timeout);
		});
		it('should leave room', function (done) {
			var response = '';
			
			clients[0].write(leaveCommand + eol);
			
			clients[0].on('data', function (data) {
				response += data.toString();
			});
			
			setTimeout(function () {
				response.should.endWith(util.format(core.texts.userLeavingRoom, roomNames[0]) + eol);
				
				var room = core.findBy(core.rooms, 'name', roomNames[0]);
				(room !== undefined).should.be.true;
				room.members.length.should.equal(0);
				
				var user = core.findBy(core.users, 'name', userPrefix + 0);
				(user !== undefined).should.be.true;
				(user.room !== undefined).should.be.false;
				user.state.should.equal(user.states.logged);
				
				done();
			}, timeout);
		});
	});
	
	describe('#join and talk', function () {
		it('should join room', function (done) {
			clients.forEach(function (client) {
				client.write(joinCommand + ' ' + roomNames[0] + eol);
			});
			
			var room = core.findBy(core.rooms, 'name', roomNames[0]);
			(room !== undefined).should.be.true;
			room.members.length.should.equal(0);
			
			setTimeout(function () {
				room.members.length.should.equal(clients.length);
				
				clients.forEach(function (client, i) {
					var user = core.findBy(core.users, 'name', userPrefix + i);
					(user !== undefined).should.be.true;
					user.room.should.equal(room);
					user.state.should.equal(user.states.inRoom);
				});
				
				done();
			}, timeout);
		});
		
		it('should talk in room', function (done) {
			var responses = [];
			var messageFormat = 'Hello, I am client #%s';
			
			var outsiderName = 'outsider';
			outsider = net.connect({ port: port }, function() {
				this.write(outsiderName + eol);
			});
			
			responses[clientsAmount] = '';
			outsider.on('data', function (data) {
				responses[clientsAmount] += data;
			});
			
			
			clients.forEach(function (client, i) {
				responses[i] = '';
				client.on('data', function (data) {
					responses[i] += data.toString();
				});
				
				client.write(util.format(messageFormat, i) + eol);
			});
			
			setTimeout(function () {
				//check if all clients in room got the messages
				clients.forEach(function (client, i) { // prepare message
					var saysValidator = new RegExp(util.format(core.texts.roomUserSays, userPrefix + i, util.format(messageFormat, i)) + eol);
					
					clients.forEach(function (client, i) { //check client
						responses[i].should.match(saysValidator);
					});
					
					responses[clientsAmount].should.not.match(saysValidator);
				});
				
				var room = core.findBy(core.rooms, 'name', roomNames[0]);
				(room !== undefined).should.be.true;
				room.members.length.should.equal(clientsAmount);
				
				done();
			}, timeout);
		});
	});
});
