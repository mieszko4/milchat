var should = require('should');
var net = require('net');

var core = require('./../lib/milchat-core');

describe('Core', function () {
	var port = 9398;
	var domain = 'milosz.ch';
	var eol = '\r\n';
	var quitCommand = '/quit';
	
	before(function () {
		core.init(port, domain);
	});
	
	after(function () {
		core.server.close();
	});
	
	describe('#connect', function () {
		it('should be able to connect', function (done) {
			var client = net.connect({ port: port }, function() {});
			
			client.on('data', function (data) {
				data.toString().should.equal(core.texts.welcome + eol);
				done();
			});
		});
	});
	
	describe('#quit', function () {
		it('should be able to quit', function (done) {
			var response = '';
			var client = net.connect({ port: port }, function() {
				client.write(quitCommand + eol);
			});
			
			client.on('data', function (data) {
				response += data.toString();
			});
			
			client.on('end', function() {
				response.should.endWith(core.texts.bye + eol);
				done();
			});
		});
		
		it('should be able to quit many clients', function (done) {
			var clientsAmount = 10;
			var quitersAmount = 10;
			var quiterCounter = 0;
			
			for (var i = 0; i < clientsAmount; i++) {
				var response = '';
				var client = net.connect({ port: port }, function() {
					if (quiterCounter < quitersAmount) {
						this.write(quitCommand + eol);
						quiterCounter++;
					}
				});
			
				client.on('data', function (data) {
					response += data.toString();
				});
			
				client.on('end', function() {
					response.should.startWith(core.texts.welcome + eol);
					response.should.endWith(core.texts.bye + eol);
					quiterCounter--;
					
					if (quiterCounter <= 0) {
						done();
					}
				});
			}
		});
		
		it('should be able to quit some clients', function (done) {
			var clientsAmount = 10;
			var quitersAmount = 7;
			var quiterCounter = 0;
			
			for (var i = 0; i < clientsAmount; i++) {
				var response = '';
				var client = net.connect({ port: port }, function() {
					if (quiterCounter < quitersAmount) {
						this.write(quitCommand + eol);
						quiterCounter++;
					}
				});
			
				client.on('data', function (data) {
					response += data.toString();
				});
			
				client.on('end', function() {
					response.should.startWith(core.texts.welcome + eol);
					response.should.endWith(core.texts.bye + eol);
					quiterCounter--;
					
					if (quiterCounter <= 0) {
						done();
					}
				});
			}
		});
	});
});
