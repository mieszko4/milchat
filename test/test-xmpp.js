var should = require('should');
var net = require('net');
var util = require('util');
var JID = require('node-xmpp').JID;

var core = require('./../lib/milchat-core');
var login = require('./../lib/milchat-login');
var rooms = require('./../lib/milchat-rooms');
var chat = require('./../lib/milchat-chat');
var xmpp = require('./../lib/milchat-xmpp');

describe('Xmpp', function () {
	var port = 9398;
	var xmppPort = 5268; //standard port: 5269
	var domain = 'milosz.ch';
	var eol = '\r\n';
	var chatCommand = '/chat';
	var leaveCommand = '/leave';
	var timeout = 100;
	var userPrefix = 'milosz_';
	var clients = [];
	var clientsAmount = 5;
	
	before(function (done) {
		core.init(port, domain);
		login.init(core);
		rooms.init(core, []);
		chat.init(core);
		xmpp.init(core, xmppPort);
		
		core.users.length.should.equal(0);
		for (var i = 0; i < clientsAmount; i++) {
			(function (i) {
				var name = userPrefix + i;
				clients[i] = net.connect({ port: port }, function() {
					this.write(name + eol);
				});
			}(i));
		}
		
		setTimeout(function () {
			core.users.length.should.equal(clientsAmount);
			done();
		}, timeout);
	});
	
	after(function () {
		core.server.close();
		////core.xmppRouter.close();
	});
	
	describe('#chat', function () {
		it('should join chat with outside user', function (done) {
			var response = '';
			var chatterName = 'milosz@jwchat.org';
			
			var user = core.findBy(core.users, 'name', userPrefix + 0);
			(user !== undefined).should.be.true;
			user.state.should.equal(user.states.logged);
			
			var chatter = core.findBy(core.users, 'name', chatterName);
			(chatter !== undefined).should.be.false;
			
			clients[0].write(chatCommand + ' ' + chatterName + eol);
			
			clients[0].on('data', function (data) {
				response += data.toString();
			});
			
			setTimeout(function () {
				user.state.should.equal(user.states.inChat);
				user.chatter.should.be.instanceof(JID);
				response.should.endWith(util.format(core.texts.userJoinedChat, user.chatter.toString()) + eol);
				done();
			}, timeout * 10); //wait for service
		});
		it('should send message', function (done) {
			var response = '';
			var message = 'Hello, I am chatter from milosz.ch';
			
			var user = core.findBy(core.users, 'name', userPrefix + 0);
			var chatterName = user.chatter.toString();
			
			clients[0].write(message + eol);
			
			clients[0].on('data', function (data) {
				response += data.toString();
			});
			
			setTimeout(function () {
				user.state.should.equal(user.states.inChat);
				response.should.be.empty;
				done();
			}, timeout * 10); //wait for response
		});
		it('should leave chat with known user', function (done) {
			var response = '';
			
			var user = core.findBy(core.users, 'name', userPrefix + 0);
			var chatterName = user.chatter.toString();
			
			clients[0].write(leaveCommand + eol);
			
			clients[0].on('data', function (data) {
				response += data.toString();
			});
			
			setTimeout(function () {
				user.state.should.equal(user.states.logged);
				
				response.should.endWith(util.format(core.texts.userLeavingChat, chatterName) + eol);
				done();
			}, timeout);
		});
	});
});
