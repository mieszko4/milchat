module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		jshint: {
			files: ['Gruntfile.js', 'index.js', 'lib/**/*.js', 'test/**/*.js'],
			options: {
				// options here to override JSHint defaults
				globals: {
					jQuery: true,
					console: true,
					module: true,
					document: true
				}
			}
		},
		replace: {
			dist: {
				options: {
					patterns: [
						{
							match: 'path',
							replacement: __dirname
						}
					]
				},
				files: [
					{src: ['index.crontab'], dest: '/etc/cron.d/milchat'}
				]
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-replace');

	grunt.registerTask('install-crontab', ['replace']);
	grunt.registerTask('default', ['jshint']);
};