var core = require('./lib/milchat-core');
var login = require('./lib/milchat-login');
var rooms = require('./lib/milchat-rooms');
var chat = require('./lib/milchat-chat');
var xmpp = require('./lib/milchat-xmpp');
var websocket = require('./lib/milchat-websocket');

core.init(9399, 'milosz.ch');
login.init(core);
rooms.init(core, ['chat', 'hottab']);
chat.init(core);
xmpp.init(core);
websocket.init(core, 9400);