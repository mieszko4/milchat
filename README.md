# Milchat #

A simple telnet chat implementation.

### Demo ###

Run ```telnet milosz.ch 9399``` for live telnet demo.
Or go to [this page](http://milosz.ch:9400/) for WebSocket telnet extension live demo.

### Features ###

* Login
* Rooms
* Chat (private)
* XMPP federation
* WebSocket extension

### Installation ###

* Download repository with ```git clone```
* Install nodejs dependencies with ```npm install```
* Run server with ```node index.js```

### Usage example ###

* ```telnet localhost 9399```

>Trying ::1...  
>Trying 127.0.0.1...  
>Connected to localhost.  
>Escape character is '^]'.  
>Welcome to the Milchat server  
>Login Name?  

* ```Miłosz```  

>Welcome Miłosz!

* ```/rooms```  

>Active rooms are  
>\* chat (0)  
>\* hottab (0)  
>end of list  

* ```/join chat```

>entering room: chat  
>\* Miłosz (\*\* this is you)  
>end of list  

* ```Hello!```

>Miłosz: Hello!    

* ```/leave```

>leaving room: chat    

* ```/chat milosz@jwchat.org```

>you joined private chat with: milosz@jwchat.org    

* ```Hello?```

>Sorry, chatter did not get your message    

* ```/quit```

>BYE    
>Connection closed by foreign host.