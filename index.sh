#!/bin/sh
if ps -ef | grep -v grep | grep "$1/index.js" ; then
	exit 0
else
	/usr/local/bin/node "$1/index.js" >> "$1/milchat.log" 2>&1 &
	exit 0
fi
